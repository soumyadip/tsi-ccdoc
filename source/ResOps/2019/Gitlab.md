# Gitlab exercises #

You can use either **gitlab.com** or **gitlab.ebi.ac.uk**, if you have a login there. Both run the Enterprise Edition of gitlab. The EBI instance requires two-factor authentication (2FA), which complicates things. You'll need an accessory app that can generate authentication tokens on your personal devices (phone, tablet, laptop). If you want to generate tokens on your laptop, [Authy](https://authy.com/) is a good choice.

To make life easier for these exercises, we're going to use **gitlab.com** in all the examples. Once you set up 2FA on **gitlab.ebi.ac.uk** you should be able to run there too, just as easily. You'll have to change a few hostnames, but that's all.

Also, you can run these exercises on your laptop, or on the virtual machine we provide for the course. We recommend using the virtual machine for consistency, but it's your choice.

There are several separate exercises, you can follow them in sequence or, if you prefer, jump straight to the exercise that interests you. For the most part, they're in the form of annotated command-line sessions. You can copy/paste the commands straight from the screen to your terminal, but you'll have to be aware of things that change - user names, process IDs etc.

The easy way to get access to the example files used in the exercises is to clone this repository from the command line:

```
# Clone this documentation if you haven't already done so
> cd
> git clone https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc
> cd tsi-ccdoc/tsi-cc/ResOps/scripts/gitlab
```

The exercises are:
 - [Log into gitlab, set up your account](gitlab/01_gitlab-account-setup.md)
 - [Create a new project and import the example code](gitlab/02_create-sample-project.md)
 - [Download and run the docker image from your gitlab project](gitlab/03_run-docker-image.md)
 - [Use git tags to create a named version of a docker image](gitlab/04_using-git-tags.md)
 - [Extend the pipeline by adding further steps](gitlab/05_add-further-steps.md)
 - [Change the order of the pipeline steps](gitlab/06_change-order-of-steps.md)
 - [Pass secret information to the build pipeline](gitlab/07_pass-build-secrets.md)
 - [Learn about AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/).