Error-Tracking
--------------

.. contents::

Why Error-Tracking is required
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- Getting rid of risks to deploy code with bugs
- Helping QA with testing code
- Getting a quick notification about troubles
- Allowing a fast turnaround with bug fixes
- Receiving a convenient display errors in admin panel
- Sorting the errors by user / browser segments

Implementation
~~~~~~~~~~~~~~~
Implementation of Error-tracking,

https://gitlab.ebi.ac.uk/soumyadip/error-tracking-example

Deployed resource,
http://sentry.rdsds.ci-apps.tsi.ebi.ac.uk/ [sample@ebi.ac.uk/sample or Register]

Gitlab Integration
~~~~~~~~~~~~~~~~~~~
Gitlab Integration information can be found at,

https://docs.gitlab.com/ee/user/project/operations/error_tracking.html