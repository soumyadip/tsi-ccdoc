Tracing
--------

.. contents::

Why Tracing is required
~~~~~~~~~~~~~~~~~~~~~~~~
As on-the-ground microservice practitioners are quickly realizing, the majority of operational 
problems that arise when moving to a distributed architecture are ultimately grounded in two 
areas: networking and observability. It is simply an orders of magnitude larger problem to network 
and debug a set of intertwined distributed services versus a single monolithic application.

What Tracing addresses
~~~~~~~~~~~~~~~~~~~~~~~~
- Distributed transaction monitoring
- Performance and latency optimization
- Root cause analysis
- Service dependency analysis
- Distributed context propagation

Implementation
~~~~~~~~~~~~~~~
Implementation of tracing (Opentracing - Jaegar) can be found in,

https://gitlab.ebi.ac.uk/soumyadip/opentracing-sample-python/

The Jaegar UI is available at,

http://jaegar.rdsds.ci-apps.tsi.ebi.ac.uk/search

Gitlab Integration
~~~~~~~~~~~~~~~~~~~

Gitlab Integration information can be found at,

https://docs.gitlab.com/ee/user/project/operations/tracing.html