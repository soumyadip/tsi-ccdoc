Agenda for ResOps 2019
======================

Dates & booking information
---------------------------

The dates reserved for 2019 are:

+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
|  Date          | Location        | #places | Registration link                                        | Registration Close  |
+================+=================+=========+==========================================================+=====================+
| 21 Aug 2019    | Garden Room     |    30   |    `21 Aug Register <http://bit.ly/cc-resops-aug19>`_    | closed              |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 04 Sep 2019    | Training room 1 |    30   |    `04 Sep Register <http://bit.ly/cc-resops-190904>`_   | closed              |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 13 Sep 2019    | Garden Room     |    30   |    `13 Sep Register <http://bit.ly/cc-resops-190913>`_   | closed              |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 02 Oct 2019    | Garden Room     |    30   |    `02 Oct Register <http://bit.ly/cc-resops-191002>`_   | 22 Sep 2019         |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+
| 18 Oct 2019    | Garden Room     |    30   |    `18 Oct Register <http://bit.ly/cc-resops-191018>`_   | 08 Oct 2019         |
+----------------+-----------------+---------+----------------------------------------------------------+---------------------+

Bookings are managed via eventbrite. If you're interested in a particular date and it's already full, please add your name to the waiting list, we'll notify you if a place becomes available.

If we don't get enough bookings for a particular date we may cancel it and ask people to re-subscribe for a different session.

If all the courses fill up we will schedule more. We may also increase the number of places available on the course as we gain experience with it, so again, make use of the waiting list.

Course objectives
-----------------

This workshop will provide some background to cloud computing and practical experience in building, deploying and running applications in cloud platforms - Embassy (OpenStack), Google, Amazon and Azure. Using examples drawn from EMBL-EBI’s experiences in the life-sciences, but using tools and technologies that are cross-platform and cross-domain, attendees will come away with a knowledge as to the user-centric hybrid cloud strategy as well as practical aspects of deploying across different clouds and the architectural considerations around porting applications to a cloud.

Prerequisite
------------

* Must bring your laptops, we can’t provide machines.
* Your laptop should have an SSH client(for window user Putty or any choice e.g. WDL) and a valid public SSH key.
* You should be comfortable with basic Linux command-line usage and have a working knowledge of GIT.
* Account should be created on `Public GitLab <https://gitlab.com/users/sign_in>`_ to avoid 2F authentication from EBI gitlab.
* We are providing sandbox over embassy cloud to play exercises. Workshop participants can use Nano editor by default. Other CLI editors are also available in the sandbox.

Schedule and links
------------------

+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   Time   | Duration | Topic                                                                                                              |
+==========+==========+====================================================================================================================+
|   09.00  | 15 min   | Setting up, signing in.                                                                                            |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   09.15  | 15 min   | `Introduction to ResOps, Day schedule <../../_static/pdf/resops2019/Introduction.pdf>`_                            |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   09.30  | 15 min   | `Cloud 101 <../../_static/pdf/resops2019/Cloud-101_V1.6.pdf>`_                                                     |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   09:45  | 20 min   | `Porting apps into clouds <../../_static/pdf/resops2019/PortingResearchPipelinesintoClouds.pdf>`_                  |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   10:05  | 40 min   | `Creating Containers with Docker <../../_static/pdf/resops2019/Creating-Containers-with-Docker.pdf>`_              |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   10.45  | 10 min   |  Break                                                                                                             |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   10.55  | 40 min   | `Docker Practical <Docker.html>`_                                                                                  |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   11.35  | 35 min   | `Introduction to Gitlab <../../_static/pdf/resops2019/Introduction-to-Gitlab.pdf>`_                                |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   12.10  | 30 min   | `Gitlab Practical <Gitlab.html>`_                                                                                  |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   12.40  | 45 min   | Lunch                                                                                                              |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   13.25  | 35 min   | `Kubernetes 101 <../../_static/pdf/resops2019/Kubernetes-101.pdf>`_                                                |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   14:00  | 25 min   | `Embassy Hosted Kubernetes (Demo) <Kubernetes-Demo-2019.html>`_                                                    |
|          |          | / `Overview <../../_static/pdf/resops2019/KubernetesPracticals.pdf>`_                                              |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   14.25  | 45 min   | `Kubernetes Practical <Minikube-and-NGINX-Practical-2019.html>`_                                                   |
|          |          | / `Overview <../../_static/pdf/resops2019/KubernetesPracticals.pdf>`_                                              |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   15.10  | 15 min   | Break                                                                                                              |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   15.25  | 60 min   | `Advanced Kubernetes Practical <Scaling-up-Kubernetes.html>`_                                                      |
|          |          | / `Overview <../../_static/pdf/resops2019/KubernetesPracticals.pdf>`_                                              |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
|   16:25  | 15 min   | Summary & Feedback                                                                                                 |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
| Homework |          | `Advanced Kubernetes Reading <Important-considerations-for-research-pipelines.html>`_                              |
|          |          | / `Overview <../../_static/pdf/resops2019/KubernetesPracticals.pdf>`_                                              |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+
| Homework |          | Continue with the exercises                                                                                        |
+----------+----------+--------------------------------------------------------------------------------------------------------------------+

.. +-------+----------+--------------------------------------------------------------------------------------------------------------------+
.. | 11.30 | 30 min   | `Case study of porting Rfam into K8S <../../_static/pdf/resops2019/CaseStudyofPortingRfamintoCloud.pdf>`_          |

These resources are being developed as part of the EOSC-Life project. EOSC-Life has received funding from the European Union’s Horizon 2020 programme under grant agreement number 824087.

