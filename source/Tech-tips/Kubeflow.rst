Kubeflow for Machine Learning
=============================

Kubeflow is a cloud-native platform for machine learning based on Google's internal ML pipelines. The complete documentation is at `https://www.kubeflow.org/docs/ <https://www.kubeflow.org/docs/>`_.

Kubeflow is under active development. There is one new release roughly every month. Here is a link to the release notes: `https://github.com/kubeflow/kubeflow/releases/ <https://github.com/kubeflow/kubeflow/releases/>`_.

Setting up Kubeflow on GKE
--------------------------

Kubeflow can run on any environment with Kubernetes. If it is used for ML, model, quota and performance of GPUs become a major decision factor. Embassy Hosted Kubernetes does not have GPUs. GKE is tried first as it is the most mature environment for Kubernetes, Kubeflow and ML with GPU acceleration.

1. Create a GKE cluster. Choice of a zone is important if GPUs are needed. See `GPU Quota at europe-west`_ for GPU accelerators in europe-west.
2. Follow instructions at https://www.kubeflow.org/docs/started/k8s/kfctl-existing-arrikto/ to deploy Kubeflow. It requires a Kubernetes cluster with LoadBalancer support. Create MetalLB if needed.
3. Get the IP address and open Kubeflow dashboard.

Accessing Kubeflow with the following commands::

  gcloud container clusters get-credentials ${CLUSTER} --zone ${ZONE} --project ${PROJECT}
  IP_KUBEFLOW=$( kubectl get svc -n istio-system istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}' )
  open https://${IP_KUBEFLOW}

Setting up Jupyter notebook
---------------------------

Jupyter notebooks can be created easily on the Kubeflow dashboard.

1. Click `Notebook Servers` to create as many as you want.
2. Click `CONNECT` to start using a notebook server.

GPU Quota at europe-west
------------------------

To see a list of all GPU accelerator types supported in each zone, run the following command::

  gcloud compute accelerator-types list | grep europe-west

  nvidia-tesla-k80       europe-west1-d             NVIDIA Tesla K80
  nvidia-tesla-p100      europe-west1-d             NVIDIA Tesla P100
  nvidia-tesla-p100-vws  europe-west1-d             NVIDIA Tesla P100 Virtual Workstation
  nvidia-tesla-k80       europe-west1-b             NVIDIA Tesla K80
  nvidia-tesla-p100      europe-west1-b             NVIDIA Tesla P100
  nvidia-tesla-p100-vws  europe-west1-b             NVIDIA Tesla P100 Virtual Workstation
  nvidia-tesla-p100      europe-west4-a             NVIDIA Tesla P100
  nvidia-tesla-p100-vws  europe-west4-a             NVIDIA Tesla P100 Virtual Workstation
  nvidia-tesla-v100      europe-west4-a             NVIDIA Tesla V100
  nvidia-tesla-p4        europe-west4-c             NVIDIA Tesla P4
  nvidia-tesla-p4-vws    europe-west4-c             NVIDIA Tesla P4 Virtual Workstation
  nvidia-tesla-t4        europe-west4-c             NVIDIA Tesla T4
  nvidia-tesla-t4-vws    europe-west4-c             NVIDIA Tesla T4 Virtual Workstation
  nvidia-tesla-v100      europe-west4-c             NVIDIA Tesla V100
  nvidia-tesla-p4        europe-west4-b             NVIDIA Tesla P4
  nvidia-tesla-p4-vws    europe-west4-b             NVIDIA Tesla P4 Virtual Workstation
  nvidia-tesla-t4        europe-west4-b             NVIDIA Tesla T4
  nvidia-tesla-t4-vws    europe-west4-b             NVIDIA Tesla T4 Virtual Workstation
  nvidia-tesla-v100      europe-west4-b             NVIDIA Tesla V100

In summary, the following models are availabe as permanent or preemptible at present:

#. NVIDIA K80
#. NVIDIA V100
#. NVIDIA P100
#. NVIDIA P4
#. NVIDIA T4

Note that P100, P4 and T4 are also available as virtual workstation.

According to `https://cloud.google.com/kubernetes-engine/docs/how-to/gpus <https://cloud.google.com/kubernetes-engine/docs/how-to/gpus>`_, GKE nodepools can be created with all the GPUs above.

#. Kubernetes version > 1.9 for Container-optimised OS or > 1.11.3 for Ubuntu node image
#. Manually install GUP driver via a DaemonSet `https://cloud.google.com/kubernetes-engine/docs/how-to/gpus#installing_drivers <https://cloud.google.com/kubernetes-engine/docs/how-to/gpus#installing_drivers>`_

GPUs are used on accelerators to VMs or Kubernetes clusters. The virtual infrastructure passes requests through to GPUs on the same region / zone. GPUs are now availble on all regions and zones. However, quota varies. If a workload requires GPU, GPU quota needs to be checked when a region / zone is selected.

GPU pricing
-----------

GPU pricing seems the same in all regions. More details can be found at `https://cloud.google.com/compute/all-pricing#gpus <https://cloud.google.com/compute/all-pricing#gpus>`_.

Regions and zones `https://cloud.google.com/compute/docs/regions-zones/ <https://cloud.google.com/compute/docs/regions-zones/>`_

References
----------

* https://cloud.google.com/gpu/
* https://cloud.google.com/compute/docs/gpus/
* https://www.kubeflow.org/docs/started/k8s/kfctl-existing-arrikto/
* https://www.kubeflow.org/docs/