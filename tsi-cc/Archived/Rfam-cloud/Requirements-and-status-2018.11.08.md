## Requirements and status 2018.11.08

### Diagram on Lucid Chart

[Rfam architecture on ECP](https://www.lucidchart.com/invitations/accept/823b5841-460c-422a-9240-29793a01cf54)

### Major suggestions

Hi David,

Thank you very much for working on this. 

Something minor that caught my eye is a typo on step 2, change Depoly to Deploy?

Here are a few comments from my perspective: 

We have 2 types of people who access the k8s cluster:
1. The Rfam k8s admin
2. An Rfam curator
*Rfam curator actions:*
1. Request a new account
2. ssh to K8s Master node
   1. This action gives the user direct access to the running pod (entry pod) with a kubectl exec command embedded in the `~/.bashrc` file 
3. Use the pipeline

**Note**: The pipeline will be modified in such a way that it communicates with the k8s server to create more pods depending on which tools are being used. 

*Rfam k8s cluster admin:*
1. Creates a new user account
   1. Create a new linux user using useradd
   2. Grand the new user the correct permissions to run the pipeline
   3. Create a user PV
   4. Create a user PVC
   5. Create an entry pod for the new user

**Note**: All steps required for the creation of a user account can be wrapped up in a script 

The k8s master orchestrates everything else (i.e scheduling of pods on available nodes, ensuring that containers in a pod are running). 

I’m also thinking of deploying an edge node to alleviate the Master from extra workload. As the number of users increases in the future, we can scale this horizontally, meaning increasing the number of execution nodes, edge nodes and perhaps more master nodes too to handle the scheduling. 

Do the comments make sense? Please feel free to let me know if there’s anything that is unclear. 

Kind regards,
Ioanna

Ioanna  Kalvari
Software Developer - Rfam
European Bioinformatics Institute (EMBL-EBI)

Tel:	+ 44 (0) 1223 494279
Fax:	+ 44 (0) 1223 494468
Email: ikalvari@ebi.ac.uk